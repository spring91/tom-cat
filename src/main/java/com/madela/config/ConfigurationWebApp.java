package com.madela.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Класс конфигурации нашего приложения
 * <p>
 * - @EnableWebMvc импортирует Spring MVC конфигурацию из WebMvcConfigurationSupport.
 * - @ComponentScan делает доступным для IoC контейнера spring все наши аннотированные компоненты.
 */
@EnableWebMvc
@Configuration
@ComponentScan("com.madela")
public class ConfigurationWebApp {
}

