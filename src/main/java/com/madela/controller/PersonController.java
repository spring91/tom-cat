package com.madela.controller;

import com.madela.dao.PersonDAO;
import com.madela.dao.impl.PersonDAOImpl;
import com.madela.model.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Controller
@ResponseBody
@RequestMapping("/persons")
public class PersonController {

    private final PersonDAO personDAO;

    @Autowired
    public PersonController(PersonDAOImpl personDAO) {
        this.personDAO = personDAO;
    }


    @GetMapping
    public List<Person> getAll() {
        log.info("GET:/persons");

        return personDAO.getAll();
    }

    @GetMapping("/{id}")
    public Person getById(@PathVariable("id") Long id) {
        log.info("GET:/persons/{id} : id={}", id);

        return personDAO.getById(id);
    }

    @PostMapping
    public ResponseEntity<?> create(Person person) {
        log.info("POST:/persons : person={}", person);

        personDAO.save(person);
        return ResponseEntity.ok(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable("id") Long id, Person person) {
        log.info("PUT:/persons/{id} : id={}, person={}", id, person);

        personDAO.update(id, person);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        log.info("DELETE:/persons/{id} : id={}", id);

        personDAO.delete(id);
        return ResponseEntity.accepted().build();
    }
}
