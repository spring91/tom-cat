# Разработать Spring приложение c базовыми CRUD операциями.

Проект сделан на чистом Spring и JDBC, с использованием PostgreSQL в кач-ве базы данных. Докер не использовался.
Реализованы базовые операции CRUD для работы с БД, в кач-ве инструмента проверки запросов использовался Postman.

Установка и ручная настройка TomCat через сборщик Maven, а так же утилита запросов Postman:

- [maven](info/collector/maven.md)
- [instruction_tomcat](info/web/install_tomcat/instruction.md)
- [postman](info/web/postman/postman.md)

Понятие Диспатчер-Сервлета в MVC технологии Spring:

- [dispatcher_servlet](info/web/dispatcher_servlet/MVC.md)
- [Filters_and_Interceptors](info/web/dispatcher_servlet/Filters_and_Interceptors.md)

Общие представления об архитектуре приложении:

- [CRUD](info/bd/crud.md)

JDBC Connection к базе, а так же ее настройка:

- [JDBC](info/bd/jdbc%20/jdbc.md)