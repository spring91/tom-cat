package com.madela.dao;

import com.madela.model.Person;

import java.util.List;

public interface PersonDAO {

    /**
     * Метод получения сущности по ID
     *
     * @param id номер сущности
     * @return найденная сущность Person
     */
    Person getById(Long id);

    /**
     * Метод получения всех сущностей
     *
     * @return списко сущностей типа Person
     */
    List<Person> getAll();

    /**
     * Метод сохранения сущности в БД
     *
     * @param person сохраняемая сущность
     */
    void save(Person person);

    /**
     * Метод обновления сущности в БД
     *
     * @param id номер обновляемой сущности
     * @param updatedPerson нова сохраняемая сущность
     */
    void update(Long id, Person updatedPerson);

    /**
     * Метод удаления сущности в БД
     *
     * @param id номер удаляемой сущности
     */
    void delete(Long id);
}
