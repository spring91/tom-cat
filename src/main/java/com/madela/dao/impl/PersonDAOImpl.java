package com.madela.dao.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import com.madela.dao.PersonDAO;
import com.madela.model.Person;

import java.lang.reflect.InvocationTargetException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

@Slf4j
@Component
public class PersonDAOImpl implements PersonDAO {
    //TODO: сделать логирование в файл

    //yml прикручивать больше мороки(
    private static final String POSTGRESQL_JDBC_DRIVER = "org.postgresql.Driver";
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/jdbc_registration";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "pgadminpsgr";

    private static final String INSERT_PERSON = "INSERT INTO person VALUES (?,?);";
    private static final String GET_PERSON = "SELECT * FROM person WHERE id=?";
    private static final String GET_ALL_PERSONS = "SELECT * FROM person;";
    private static final String UPDATE_PERSON = "UPDATE person SET firstName=? WHERE id=?;";
    private static final String DELETE_PERSON = "DELETE FROM person WHERE id=?";

    static {
        try {
            Class.forName(POSTGRESQL_JDBC_DRIVER).getDeclaredConstructor().newInstance();
        } catch (InstantiationException
                 | ClassNotFoundException
                 | NoSuchMethodException
                 | InvocationTargetException
                 | IllegalAccessException e) {
            log.error(e.getMessage());
        }
    }

    public Person getById(Long id) {
        Person person = null;
        try (var connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
             var preparedStatement = connection.prepareStatement(GET_PERSON)) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                person = Person.builder()
                        .id(resultSet.getLong("id"))
                        .name(resultSet.getString("firstName"))
                        .build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return person;
    }

    public List<Person> getAll() {
        var people = new LinkedList<Person>();
        try (var connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
             var preparedStatement = connection.prepareStatement(GET_ALL_PERSONS)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                var person = Person.builder()
                        .id(resultSet.getLong("id"))
                        .name(resultSet.getString("firstName"))
                        .build();
                people.add(person);
            }
        } catch (SQLException e) {
            log.warn(e.getMessage());
        }
        return people;
    }

    public void save(Person person) {
        //primary key присваивается автоматически(см sql скрипт)
        //Connection открываем при обращении и сразу закрываем
        try (var connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
             var preparedStatement = connection.prepareStatement(INSERT_PERSON)) {
            preparedStatement.setLong(1, person.getId());
            preparedStatement.setString(2, person.getName());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.warn(e.getMessage());
        }
    }

    public void update(Long id, Person updatedPerson) {
        try (var connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
             var preparedStatement = connection.prepareStatement(UPDATE_PERSON)) {
            preparedStatement.setString(1, updatedPerson.getName());
            preparedStatement.setLong(2, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.warn(e.getMessage());
        }
    }

    public void delete(Long id) {
        try (var connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
             var preparedStatement = connection.prepareStatement(DELETE_PERSON)) {
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.warn(e.getMessage());
        }
    }
}
